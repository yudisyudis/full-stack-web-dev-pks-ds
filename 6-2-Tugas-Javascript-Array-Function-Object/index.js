//SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 1; i < daftarHewan.length; i++)
    for (var j = 0; j < i; j++)
        if (daftarHewan[i] < daftarHewan[j]) {
          var x = daftarHewan[i];
          daftarHewan[i] = daftarHewan[j];
          daftarHewan[j] = x;
        }

console.log(daftarHewan);

//SOAL 2
function introduce(data) {
    return ("Nama saya "+data.name+", umur saya "+data.age+", alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby+".")

}



// Jalankan function introduce


var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 




//SOAL 3

// menghitung huruf vokal

function hitung_huruf_vokal(str) { 

    const count = str.match(/[aeiou]/gi).length;

    return count;
}

// input
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2
