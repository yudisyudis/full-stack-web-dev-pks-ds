//SOAL 1

console.log("Fungsi Luas:")
// fungsi luas
const luas = (a,b) => {
    return a*b;
  } 

console.log(luas(5,4));

console.log("Fungsi Keliling:")

//fungsi keliling
const keliling = (a,b) => {
    return 2*(a+b);
  } 

console.log(keliling(5,5));

//SOAL 2
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  console.log("Jawaban Soal Nomor 2:")

  newFunction("William", "Imoh").fullName() 

  //SOAL 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const {firstName, lastName, address, hobby} = newObject

//Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)


// Driver code
console.log("Jawaban Soal Nomor 3:")

console.log("nama: "+firstName, lastName+" alamat: "+address+" hobi: "+hobby)

//SOAL 4
//Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log('Jawaban Soal Nomor 4:')   

console.log(combined)   

//SOAL 5
//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `
console.log('Jawaban Soal Nomor 5:')   

console.log(before)   

  