<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

//model user
class User extends Model
{
    protected $fillable = ['id', 'nama', 'role_id', 'email', 'username', 'password', 'email_verified_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model -> role_id = Role::where('role', 'admin')->first()->id;
            
        });
    }

    public function roles()
    {
        return $this->belongsTo('App\Role');
    }

    public function otp_codes()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }



}
