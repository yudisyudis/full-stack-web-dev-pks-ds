<?php
trait Hewan {

    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi() {
        echo $nama."sedang".$keahlian;
    }
}


trait Fight {

    public $attackPower;
    public $defencePower;

    public function serang()  {
        echo $nama."sedang menyerang".$nama;
    }

    public function diserang()  {
        echo $nama."sedang diserang".$nama;
    }

    
}

class Elang {
  use Hewan, Fight;
}

class Harimau {
    use Hewan, Fight;
  }

?> 